import React, { Component } from 'react'
import {Redirect} from 'react-router-dom'

import { auth } from './../firebase-config'

class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isAuth: false,
            isLogged: false,
            erro: false
        }

        this.email = null
        this.senha = null

        this.authUser = this.authUser.bind(this)
    }

    authUser() {
        this.setState({isLogged: true, erro: false})
        auth.signInWithEmailAndPassword(this.email.value, this.senha.value)
            .then(user => {
                this.setState({isAuth: true})
            })
            .catch(err => {
                this.setState({erro: true, isAuth: false, isLogged: false})
            })
    }

    render() {
        if(this.state.isAuth){
            return <Redirect to='/admin' />
        }
        return (
            <div className="container">
                <h1>Login</h1>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Email</label>
                    <input type="email" name="email" ref={ref => this.email = ref} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nome@email.com" />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" name="senha" ref={ref => this.senha = ref} className="form-control" id="exampleInputPassword1" />
                    {this.state.erro && <small id="emailHelp" className="form-text text-muted">Email or Password invalid.</small>}
                </div>
                
                <button type="button"  disabled={this.state.isLogged} className="btn btn-primary" onClick={this.authUser}>SUBMIT</button>
            </div>
        )
    }
}

export default Login