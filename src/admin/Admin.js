import React, { Component } from 'react'
import {Route, Redirect} from 'react-router-dom'

import {auth} from './../firebase-config'
import AdminMenu from './AdminMenu'
import AdminPortfolio from './AdminPortfolio'

class Admin extends Component {

    constructor(props){
        super(props)
        this.state = {
            isLogging: true,
            isAuth: false,
            user: null
        }
    }

    componentDidMount() {
        auth.onAuthStateChanged (user => {
            this.setState ({
                isLogging: false,
                isAuth: !!user,
                user
            })
        })
    }
    
    render() {
        if (this.state.isLogging) {
            return (
                <div className="container">
                    <p><span className="glyphicon glyphicon-refresh" /> ... Wait ... </p>
                </div>
            )
        }
        if (!this.state.isAuth) {
            return <Redirect to='/login' />
        }
        return

        return (
            <div>
            <h2> Admin Painel </h2>
            <Route path = {'/'} component={AdminMenu} />
            <Route path={`${this.props.match.url}/portfolio`} component={AdminPortfolio} />
            
            
            </div>
        )
    }
}

export default Admin