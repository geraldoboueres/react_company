const Rebase = require ('re-base')
const firebase = require ('firebase')

const FirebaseConfig = {
        apiKey: "AIzaSyDsY9gB1wWpbSpbkr09c31YXOLazpO2PHo",
        authDomain: "geraldo-react.firebaseapp.com",
        databaseURL: "https://geraldo-react.firebaseio.com",
        projectId: "geraldo-react",
        storageBucket: "geraldo-react.appspot.com",
        messagingSenderId: "536314013725",
        appId: "1:536314013725:web:3f95678b51e639913f3ef4"
      }
  
      const app =  firebase.initializeApp(FirebaseConfig)
      const config = Rebase.createClass(app.database())
    
      export const storage = app.storage()
      export const auth = app.auth()
    
      export default config