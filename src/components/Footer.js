import React from 'react'

const Footer = props => {

    return (
        <footer className='container-fluid text-center'>
            <a href='#myPage' title='To Top'>
                <span className='glyphicon glyphicon-chevron-up'></span>
            </a>
            <p> Project created by Geraldo Boueres transforming a standard HTML code for JSX </p>
            
        </footer>
    )

}

export default Footer 
